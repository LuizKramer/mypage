# Introdução
A proposta apresentada tem como objetivo a construção de um site html para a apresentação de um projeto feito durante o curso de Arduino apresentado pelo curso
de Engenharia de Cumputação no campus Pato Branco da UTFPR - Assim sendo escolhido pelos representates o "Semáforo".

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
|Luiz Eduardo Caldas Kramer|@LuizKramer|
|João Manoel da Luz Quevedo|@JMDLQ|
|Rafael Roveri de Castro Mendes |@Roveri|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://gitlab.com/LuizKramer/html/tree/master      
https://luizkramer.gitlab.io/html/

